__author__ = 'davide'
import os
import datetime
from os.path import expanduser
import csv,numpy
from sklearn import svm,linear_model
from sklearn import cross_validation
from sklearn import metrics

home = expanduser("~")
pathToFiles = "/Desktop/DataMiningProject/Data/";
indexfiltrato = home+pathToFiles+"index_filtrato"
testsenzaid = home+pathToFiles+"testsenzaid.csv"
testfiltered = home+pathToFiles+"testfiltered.csv"
trainsenzaid = home+pathToFiles+"trainsenzaid.csv"
trainfiltered = home+pathToFiles+"trainfiltered.csv"
trainreplicato = home+pathToFiles+"trainreplicato.csv"
submission_example = home+pathToFiles+"submission_example.csv"
output = home+pathToFiles+"out"+str(datetime.datetime.now())+".csv"
filterjar = home+"/Desktop/DataMiningProject/Filter/dist/Filter.jar"


########################
# PARAMS TO EDIT
########################

# classifier
model=linear_model.LogisticRegression(C=0.15,penalty='l1',tol=0.01, fit_intercept=True)
# number of times to replicate the train set
numerorepliche = 5
# folds of the cross validation
fold = 10 


########################
########################
########################
#REPLICA TRAIN
########################
########################
########################

#Replica train
with open(trainsenzaid,'r') as fin, open (trainreplicato,'w') as fout:
    #print(trainsenzaid)
    print("1. Sono python. Sto aprendo il file di train. "+str(datetime.datetime.now()))
    out = ""
    header = ""
    i = 0
    for row in csv.reader(fin, delimiter=','):
        if i == 0:
            header = str(row).replace('[',"").replace(']',"").replace('\'',"")+"\n"
            #print(header)
            i+=1
        else:
            out += str(row).replace('[',"").replace(']',"").replace('\'',"")+"\n"

    
    print("2. Sono python. Sto scrivendo il file di train replicato "+str(numerorepliche)+" volte "+str(datetime.datetime.now()))
    fout.write(header)
    for j in range(numerorepliche):
        if (j == numerorepliche -1 ):
            fout.write(out[:-1])
        else:
            fout.write(out)


########################
########################
########################
#CHIAMATA A JAVA
########################
########################
########################


os.system("java -jar "+filterjar+" "+trainreplicato+" "+indexfiltrato)

print("5. Sono python. Sto leggendo cosa filtrare. "+str(datetime.datetime.now()))
lines = [int(float(line.rstrip('\n'))) for line in open(indexfiltrato)]
lines.sort()
print("   Java ha selezionato "+ str(len(lines))+" attributi")

#print(lines)



########################
########################
########################
#FILTER TEST E TRAIN
########################
########################
########################

out = ""
with open(testsenzaid,'r') as fin, open (testfiltered,'w') as fout:
    print("6. Sono python. Sto aprendo il file di test. "+str(datetime.datetime.now()))
    i = 0
    for row in csv.reader(fin, delimiter=','):
        for i in range(len(lines)):
            if (i == len(lines)-1):
                out += str(row[int(float(lines[i]))-1])+"\n"

            elif (lines[i] != 0):
                out += str(row[int(float(lines[i]))-1])+","

    print("7. Sono python. Sto scrivendo il file di test. "+str(datetime.datetime.now()))
    fout.write(out)



with open(trainreplicato,'r') as fin, open (trainfiltered,'w') as fout:
    print("8. Sono python. Sto aprendo il file di train. "+str(datetime.datetime.now()))
    out = ""
    for row in csv.reader(fin, delimiter=','):
        for line in lines:
            out += str(row[int(float(line))])+","
        out = out[:-1]+'\n'
        #usate questo per aggiungere una colonna "Class" tutta di 0 altrimenti scrivete out = out[,-1]+'\n' a posto dell'if,else
    #print(out)
    print("9. Sono python. Sto scrivendo il file di train filtrato. "+str(datetime.datetime.now()))
    fout.write(out)




########################
########################
########################
#Predizione in python
########################
########################
########################


def main():
    #read in the training and test data
    train = read_csv(trainfiltered)
    #the first column of the training set will be the target for the random forest classifier
    y = [x[0] for x in train]
    X = [x[1:] for x in train]

    #print(target)
    #print(train)
    test = read_csv(testfiltered)

        # === training & metrics === #
    SEED = 42
    mean_auc = 0.0
    res = [] # register values for the std dev

    for i in range(fold):
        # for each iteration, randomly hold out 20% of the data as CV set
        X_train, X_cv, y_train, y_cv = cross_validation.train_test_split(
            X, y, test_size=.20, random_state=i*SEED)

        # if you want to perform feature selection / hyperparameter
        # optimization, this is where you want to do it

        # train model and make predictions
        model.fit(X_train, y_train)
        preds = model.predict_proba(X_cv)[:, 1]

        # compute AUC metric for this CV fold
        fpr, tpr, thresholds = metrics.roc_curve(y_cv, preds)
        roc_auc = metrics.auc(fpr, tpr)

        # mean and stdev
        mean_auc += roc_auc
        res.append(roc_auc)

    print("\nRES. Mean AUC on a %d fold CV: %.3f (+/- %.3f)\n" % (fold,mean_auc/fold,numpy.std(res)))


    model.fit(X,y)
    probs = model.predict_proba(test)
    #print(len(probs))
    #print(119748)



    with open(submission_example) as f:
        i = 0
        #print(f.readline())
        #data = "Id,Probability\n"
        data = f.readline()
        for line in f:
            line = line.strip().split(",")
            #print("line: "+str(line[0]))
            #print probs[i][0]
            data+=str(int(float(line[0])))
            data+=","+str(probs[i][0])+"\n"
            i+=1

    with open(output,"w") as f:
        f.write(data)


def read_csv(file_path, has_header = True):
    with open(file_path) as f:
        if has_header:
            f.readline()
        data = []
        for line in f:
            line = line.strip().split(",")
            data.append([float(x) for x in line])
    return data


if __name__=="__main__":
    main()