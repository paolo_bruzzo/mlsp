import csv

def main():

    #load the test set
    with open("/Users/paolobruzzo/Desktop/DataMiningProject/test.csv","rb") as source:
        rdr= csv.reader( source )
        # output file
        with open("/Users/paolobruzzo/Desktop/DataMiningProject/testFiltered.csv","wb") as result:
            wtr= csv.writer( result )
            for r in rdr:

                #replace 0,10,20... with the columns numbers you want to KEEP (NB: the first column is r[0])
                wtr.writerow( ( r[0], r[10], r[20]) )

    print("Done")

if __name__ == "__main__":
    main()