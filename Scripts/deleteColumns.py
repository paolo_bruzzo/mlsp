import csv

def main():

    #load the test set
    with open("/Users/paolobruzzo/Desktop/DataMiningProject/Backups/test.csv","rb") as source:
        rdr= csv.reader( source )
        # output file
        with open("/Users/paolobruzzo/Desktop/DataMiningProject/Backups/testsenzaid.csv","wb") as result:
            wtr= csv.writer( result )
            for r in rdr:
                del r[0:1]
                wtr.writerow( r)

    print("Done")

if __name__ == "__main__":
    main()