/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package filter;

/**
 *
 * @author davide
 */
import java.io.*;
import weka.attributeSelection.AttributeSelection;
import weka.attributeSelection.CfsSubsetEval;
import weka.attributeSelection.GreedyStepwise;
import weka.core.*;
import weka.core.converters.*;

/**
 * Generates and displays a ROC curve from a dataset. Uses a default NaiveBayes
 * to generate the ROC data.
 *
 * @author FracPete
 */
public class Filter {

    /**
     * takes one argument: dataset in ARFF format (expects class to be last
     * attribute)
     * @param args
     * @throws java.lang.Exception
     */
    public static void main(String[] args) throws Exception {
        String loadtrain;
        String DoveIndexFiltrato;
        try {
            loadtrain = args[0];
            DoveIndexFiltrato = args[1];

        } catch (Exception e) {
            String home = System.getProperty("user.home");
            String pathToFiles = "/Desktop/DataMiningProject/Data/";
            loadtrain = home + pathToFiles + "trainsenzaid.csv";
            DoveIndexFiltrato = home + pathToFiles + "index_filtrato";

        }

        CSVLoader loader = new CSVLoader();
        loader.setSource(new File(loadtrain));
        loader.setNominalAttributes("first");
        Instances data = loader.getDataSet();

        System.out.println("\n3. Sono Java : Filtering");
        System.out.println("   Numero attributi = " + data.numAttributes());
        int ind = data.numAttributes();
        System.out.println("   Class = " + data.attribute(0));

        data.setClassIndex(0);

        //System.out.println("\n3. Low-level");
        AttributeSelection attsel = new AttributeSelection();
        //PrincipalComponents eval2 = new PrincipalComponents();
        CfsSubsetEval eval2 = new CfsSubsetEval();
        
        GreedyStepwise search = new GreedyStepwise();
        //BestFirst search = new BestFirst();
        //RaceSearch search = new RaceSearch();

        //search.setSearchBackwards(true);
        attsel.setEvaluator(eval2);
        attsel.setSearch(search);
        attsel.SelectAttributes(data);
        int[] indices = attsel.selectedAttributes();
        System.out.println("   Selected " + indices.length + " attribute indices (starting with 0):\n   " + Utils.arrayToString(indices));

        try (PrintWriter out = new PrintWriter(DoveIndexFiltrato)) {
            for (int i : indices) {
                out.println(i);
            }
        }
        System.out.println("4. Sono Java: Filter done.\n");

    }

}
